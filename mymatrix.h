#ifndef MYMATRIX_H
#define MYMATRIX_H

#include <functional>
#include <vector>

using Callback = std::function<void(float)>;

class MyMatrix
{
    int cols, rows;
    std::vector<float> data;

public:
    MyMatrix();
    float getSum() const;
    float getValue(int i, int j) const;
    void calculate(Callback cb) const;
};

#endif