## Goal

Test python binding with C++ (pybind11) using a callback function.  
Test the performance of summing the values of a matrix (1000 x 1000). The matrix class with the data lives in cpp.


## Results (on my laptop)


| description                         | calculated value | time spent [sec] |
|-------------------------------------|------------------|------------|
|pure cpp (calling getSum())          | 1000000.0        | 0.0020     |
|sum in pyton by calling getValue()   | 1000000.0        | 0.8790     |
|using callback object class          | 1000000.0        | 0.2910     |
|using callback function              | 1000000.0        | 0.2270     |
|using callback function doing nothing| -                | 0.1330     |


## Install/Run:

Install python 3.6  

> git clone https://github.com/pybind/pybind11.git  
> start.bat


## Tutorial:

Shows basic setup (not the callback class):  

C++ in Python the Easy Way! #pybind11  
https://www.youtube.com/watch?v=_5T70cAXDJ0

