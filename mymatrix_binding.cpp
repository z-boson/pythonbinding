#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include "mymatrix.h"

namespace py = pybind11;


PYBIND11_MODULE(module_name, handle)
{
    handle.doc() = "doccccc";
    py::class_<MyMatrix>(handle, "PyMatrix")
        .def(py::init<>())
        .def("getSum", &MyMatrix::getSum)
        .def("getValue", &MyMatrix::getValue)
        .def("calculate", &MyMatrix::calculate);
}