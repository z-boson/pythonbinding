#include "mymatrix.h"


MyMatrix::MyMatrix()
{
    cols = 1000;
    rows = 1000;
    data.reserve(cols * rows);
    for(int i=0; i<cols; ++i)
    {
        for(int j=0; j<cols; ++j)
        {
            data.push_back(1);
        }
    }
}
float MyMatrix::getSum() const
{
    float rv = 0;
    for(int i=0; i<cols; ++i)
    {
        for(int j=0; j<cols; ++j)
        {
            rv += data[i*j+j];
        }
    }
    return rv;
}
float MyMatrix::getValue(int i, int j) const
{
    return data[i*j+j];
}
void MyMatrix::calculate(Callback cb) const
{
    for(int i=0; i<cols; ++i)
    {
        for(int j=0; j<cols; ++j)
        {
            cb(data[i*j+j]);
        }
    }
}