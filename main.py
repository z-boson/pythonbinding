import time
import _build.Release.module_name as module_name



class MyCallbackClass:
    def __init__(self):
        self.summ = 0
    def callback_py(self, ival):
        self.summ += ival

callbackObj = MyCallbackClass()
def callback_fun(ival):
    callback_fun.sum += ival
callback_fun.sum = 0

def callback_fun_do_nothing(ival):
    return


my_matrix = module_name.PyMatrix()

t0 = time.time()
getSum_value = my_matrix.getSum()
t1 = time.time()
py_sum = 0
for i in range(1000):
    for j in range(1000):
        py_sum += my_matrix.getValue(i, j)
t2 = time.time()
my_matrix.calculate(callbackObj.callback_py)
t3 = time.time()
my_matrix.calculate(callback_fun)
t4 = time.time()
my_matrix.calculate(callback_fun_do_nothing)
t5 = time.time()

def nice_elapsed_time(t0, t1):
    return f"{t1-t0:0.4f}"

print(f"cpp               Sum: {getSum_value}\ttime: {nice_elapsed_time(t0, t1)}")
print(f"py                Sum: {py_sum}\ttime: {nice_elapsed_time(t1, t2)}")
print(f"callback object   Sum: {callbackObj.summ}\ttime: {nice_elapsed_time(t2, t3)}")
print(f"callback function Sum: {callback_fun.sum}\ttime: {nice_elapsed_time(t3, t4)}")
print(f"callback dummy function: \t\ttime: {nice_elapsed_time(t4, t5)}")